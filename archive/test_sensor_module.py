from yoctopuce.yocto_api import *
from yoctopuce.yocto_genericsensor import *

errmsg = YRefParam()
if YAPI.RegisterHub("usb", errmsg) != YAPI.SUCCESS:
    sys.exit("init error " + errmsg.value)


sensor = YGenericSensor.FirstGenericSensor()
if sensor is None:
    sys.exit('No 4-20 mA sensor module found!')


# retrieve sensor module serial number
serial = sensor.get_module().get_serialNumber()
# retrieve first channel
channel1 = YGenericSensor.FindGenericSensor(serial + '.genericSensor1')


while True:
    print("channel 1: %f %s" % (channel1.get_currentValue(), channel1.get_unit()))
    time.sleep(1)
