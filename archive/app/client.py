import socket
import sys
import streamlit as st
import time
import logging

PERIOD_RERUN = 2 # Number of seconds between each rerun
DELAY_COMMAND = 1 # Minimum number of seconds to wait between commands sent to Raspberry Pi

HOST, PORT = 'EPDT-RASP15', 65432

logging.basicConfig(level='INFO', format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')


st.subheader('Control')

modes = ['Off', 'Run calibration', 'Run manual', 'Run cycles']

if 'mode_key' not in st.session_state:
    st.session_state.msg_received = ''
    st.session_state.last_msg_received = ''
    st.session_state.mode_key = modes[0]
    st.session_state.reset_mode = False
if st.session_state.reset_mode:
    st.session_state.reset_mode = False
    st.session_state.mode_key = modes[0]

mode_index = st.radio('Mode', modes, key='mode_key')
running_calibration = mode_index==modes[1]
running = mode_index==modes[2]
running_cycles = mode_index==modes[3]

direction = st.radio('Direction', ['forward', 'backward'], index=0, disabled=running_calibration)
spm_mode = st.checkbox('Set steps/min', value=True, disabled=running_calibration)
if spm_mode:
    spm = st.number_input('Steps/min', min_value=10, max_value=24*120, value=60, disabled=running_calibration)
    delay_total = 1000/(spm/60)
    delay_off = st.number_input('Delay_off (ms)', value=delay_total/2, disabled=True)
    delay_on  = st.number_input('Delay_on (ms)',  value=delay_total/2, disabled=True)
else:
    my_slot = st.empty()
    delay_off = st.number_input('Delay_off (ms)', min_value=0,            max_value=1000, value=500, disabled=running_calibration)
    delay_on  = st.number_input('Delay_on (ms)',  min_value=21-delay_off, max_value=1000, value=500, disabled=running_calibration)
    frequency = 1000/(delay_on+delay_off) # in steps/s
    motor_spm = 60*frequency
    spm = my_slot.number_input('Steps/min', value=motor_spm, disabled=True)

cycle_min = st.slider('Cycle minimum position (%)', 0, 100, disabled=running_cycles)
cycle_max = st.slider('Cycle maximum position (%)', 0, 100, disabled=running_cycles)
if cycle_max < cycle_min:
    raise Exception('Minimum position of cycle should be lower than maximum!')
cycle_time_hours = st.number_input('Cycle time in hours (optional: 0 => run cycles indefinitely)', 0.0, step=0.1, disabled=running_cycles)

# Initialize state
if 'time_last_command' not in st.session_state:
    st.session_state.time_last_command = time.time()
if 'command_is_delayed' not in st.session_state:
    st.session_state.command_is_delayed = False

config_vars = ['running', 'running_calibration', 'running_cycles', 'direction', 'delay_on', 'delay_off']
for var in config_vars:
    if var not in st.session_state:
        st.session_state[var] = -1

configuration_changed = False
for var in config_vars:
    value = globals()[var]
    if st.session_state[var] != value:
        st.session_state[var] = value
        configuration_changed = True

if configuration_changed or st.session_state.command_is_delayed:
    if time.time() - st.session_state.time_last_command < DELAY_COMMAND:
        st.session_state.command_is_delayed = True
    else:
        st.session_state.command_is_delayed = False
        st.session_state.time_last_command = time.time()
        # Send command with new configuration to Raspberry Pi
        data = f'CONFIG {running} {running_calibration} {direction} {delay_on} {delay_off} {running_cycles} {cycle_min} {cycle_max} {cycle_time_hours}'
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((HOST, PORT))
            sock.sendall(bytes(data, "utf-8"))

# Send request of status update and wait for response
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect((HOST, PORT))
    sock.sendall(bytes('STATUS', "utf-8"))
    st.session_state.last_msg_received = st.session_state.msg_received
    st.session_state.msg_received = str(sock.recv(1024), "utf-8")
    if 'Stopped' in st.session_state.msg_received and 'Stopped' not in st.session_state.last_msg_received:
        st.session_state.reset_mode = True
    

st.subheader('Raspberry Pi status')
st.write(st.session_state.msg_received)

time.sleep(PERIOD_RERUN)
st.rerun()
    
