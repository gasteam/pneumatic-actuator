import socketserver
import logging
from relay_control import RelayControl
from gpiozero import Button

switch_low =  Button(2, bounce_time=0.2)
switch_high = Button(3, bounce_time=0.2)
relay_control = RelayControl(switch_low, switch_high)

class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    
    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        msg_received = str(self.data, 'utf-8')
        logging.debug(f"{self.client_address[0]} wrote: {msg_received}")
        if 'CONFIG' in msg_received:
            logging.info(f'CONFIG message received: {msg_received}')
            self.handle_config_msg(msg_received)
        if 'STATUS' in msg_received:
            msg_status = relay_control.info_msg
            self.request.sendall(bytes(msg_status, 'utf-8'))
    
    def handle_config_msg(self, msg_received):
        _, running, running_calibration, direction, delay_on, delay_off, running_cycles, cycle_min, cycle_max, cycle_time_hours = msg_received.split(' ')
        running = running=='True'
        running_calibration = running_calibration=='True'
        forward = direction=='forward'
        delay_on = float(delay_on)
        delay_off = float(delay_off)
        running_cycles = running_cycles=='True'
        cycle_min = float(cycle_min)
        cycle_max= float(cycle_max)
        cycle_time_hours = float(cycle_time_hours)
        logging.debug(f'CONFIG msg parsed:\nrunning={running}, running_calibration={running_calibration}, forward={forward}, delay_on={delay_on}, delay_off={delay_off}, running_cycles={running_cycles}')
        if running:
            relay_control.switch(delay_on, delay_off, forward=forward)
        elif running_calibration:
            relay_control.switch(delay_on, delay_off, run_calibration_forward=True)
        elif running_cycles:
            relay_control.run_cycles(delay_on, delay_off, cycle_min, cycle_max, cycle_time_hours)
        else:
            relay_control.stop_switching()

if __name__ == "__main__":
    HOST, PORT = '', 65432
    
    logging.basicConfig(level='INFO', format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    try:
        # Create the server
        socketserver.TCPServer.allow_reuse_address = True
        server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)

        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        server.serve_forever()
    finally:
        relay_control.stop_switching()

