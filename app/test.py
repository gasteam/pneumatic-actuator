import influxdb
import os
from dotenv import load_dotenv
import time
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

load_dotenv()
influxdb_client = influxdb.InfluxDBClient(
    host = 'dbod-gasmon.cern.ch',
    port = 8081,
    username = 'admin',
    password = os.getenv('INFLUXDB_PASSWORD'),
    database = 'data',
    ssl = True,
    verify_ssl = False
)


json_body = [
    {
        "measurement": "raspberry",
        "tags": {
            "device": "epdt-rasp16",
            "setup": "pneumatic-motor"
        },
        "time": time.time_ns(),
        "fields": {
            "direction": 1
        }
    }
]
influxdb_client.write_points(json_body, protocol='json', time_precision='n')

