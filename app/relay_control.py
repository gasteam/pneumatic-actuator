
from yoctopuce.yocto_api import *
from yoctopuce.yocto_relay import *
from yoctopuce.yocto_genericsensor import *
import time, sys, logging
import threading
import influxdb
import os
from dotenv import load_dotenv
import time
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
logging.getLogger('urllib3').setLevel('INFO')
load_dotenv()

class RelayControl:
    
    def __init__(self, switch_low, switch_high, stop_all_threads_event):
        self.switch_low = switch_low
        self.switch_high = switch_high
        self.stop_all_threads_event = stop_all_threads_event
        self.logger = logging.getLogger('RelayControl')
        self.thread = threading.Thread()
        self.thread_stop_event = threading.Event()
        self.nb_relay = 0
        self.influxdb_client = self.get_influxdb_client()
        try:
            with open('data/total_count_steps.txt', 'r') as f:
                self.nb_steps_total = int(f.readline())
        except:
            self.nb_steps_total = 0
        try:
            with open('data/relay_absolute_position.txt', 'r') as f:
                self.absolute_position = int(f.readline())
        except:
            self.absolute_position = 0
        try:
            with open('data/calibration.txt', 'r') as f:
                f.readline()
                self.absolute_position_low_limit  = int(f.readline())
                self.absolute_position_high_limit = int(f.readline())
                self.potentio_current_low_limit   = float(f.readline())
                self.potentio_current_high_limit  = float(f.readline())
            self.logger.info('Calibration loaded from file!')
            self.calibration_finished = True
        except:
            self.logger.info('No calibration found on disk.')
            self.calibration_finished = False
        self.info_msg = 'Initialized'
        
        # Setup the API to use local USB devices
        errmsg = YRefParam()
        if YAPI.RegisterHub("usb", errmsg) != YAPI.SUCCESS:
            sys.exit("init error " + errmsg.value)

        relay = YRelay.FirstRelay()
        if relay is None:
            sys.exit('No relay module found!')

        serial_nb = relay.get_serialNumber()

        self.relays = []
        for i in range(1, 4):
            self.relays.append(YRelay.FindRelay(f'{serial_nb}.relay{i}'))

        sensor = YGenericSensor.FirstGenericSensor()
        if sensor is None:
            sys.exit('No 4-20 mA sensor module found!')

        # retrieve sensor module serial number
        serial = sensor.get_module().get_serialNumber()
        # retrieve first channel
        self.current_sensor = YGenericSensor.FindGenericSensor(serial + '.genericSensor1')

    def get_influxdb_client(self):
        influxdb_client = influxdb.InfluxDBClient(
            host = 'dbod-gasmon.cern.ch',
            port = 8081,
            username = 'admin',
            password = os.getenv('INFLUXDB_PASSWORD'),
            database = 'data',
            ssl = True,
            verify_ssl = False
        )
        return influxdb_client
    
    def start_thread_current_display(self):
        self.thread = threading.Thread(
            target = self.run_continuous
        )
        self.thread.start()
        self.logger.info(f'Thread started! (to display current)')
    
    def write_influxdb(self, d):
        '''Write the object to InfluxDB with the fields in the given dictionary d.'''
        json_body = [
            {
                "measurement": "raspberry",
                "tags": {
                    "device": "epdt-rasp16",
                    "setup": "pneumatic-motor"
                },
                "time": time.time_ns(),
                "fields": d
            }
        ]
        self.influxdb_client.write_points(json_body, protocol='json', time_precision='n')
    
    def run_continuous(self):
        i = 0
        while not self.stop_all_threads_event.is_set():
            # display_current
            self.ui.signal_current.emit(self.current_sensor.get_currentValue())
            i = (i+1)%999999
            if (i%600)==0:
                self.write_counter_to_file()
            if (i%2) == 0:
                # Upload to DB
                self.write_influxdb({
                    "position_percentage_steps": self.get_position_percentage(),
                    "position_percentage_potentio": self.get_potentio_position_percentage(),
                    "current": self.current_sensor.get_currentValue(),
                    "limit_switch_low": self.switch_low.is_pressed,
                    "limit_switch_high": self.switch_high.is_pressed,
                    "nb_steps_total": self.nb_steps_total
                })
            time.sleep(0.5)
        
    def reset(self):
        for relay in self.relays:
            relay.set_output(YRelay.OUTPUT_OFF)
        self.logger.debug('Reset relays finished!')
    
    def set_state(self, nb_relay, state):
        '''Set the state of relay with index 0/1/2 to the given state (True/False)'''
        self.relays[nb_relay].set_output(YRelay.OUTPUT_ON if state else YRelay.OUTPUT_OFF)

    def write_counter_to_file(self):
        with open('data/total_count_steps.txt', 'w') as f:
            f.write(str(self.nb_steps_total))

    def increment_position(self, increment, delay_on, delay_off):
        self.nb_steps_total += 1
        self.absolute_position += increment
        self.nb_relay = self.absolute_position % 3
        self.set_state(self.nb_relay, True)
        time.sleep(delay_on/1000)
        self.set_state(self.nb_relay, False)
        time.sleep(delay_off/1000)
        if self.calibration_finished:
            self.ui.signal_position.emit(self.absolute_position, self.get_position_percentage(), self.get_potentio_position_percentage())
        else:
            self.ui.signal_position.emit(self.absolute_position, -99, -99)
    
    def emit_info_msg(self, info_msg):
        self.info_msg = info_msg
        self.ui.signal_info.emit(info_msg)
    
    def switch_blocking(self, delay_on, delay_off, run_calibration_forward, run_calibration_backward, run_manual, forward, thread_stop_event):
        '''Switch the relays sequentially in the given direction (forward: 1 > 2 > 3 > 1 > ...).
        Every relay is enabled for delay_on time (in ms) with a subsequent pause of delay_off (in ms).
        The mininum value of delay_on+delay_off is 20.8 ms (this results in motor rpm of 24).'''
        if delay_on+delay_off < 20.8:
            raise Exception(f'Total delay delay_on+delay_off={delay_on+delay_off} ms is too low!')
        frequency = 1000/(delay_on+delay_off) # in Hz
        motor_spm = 60*frequency
        motor_rpm = motor_spm/120
        self.logger.info(f'Switching relays at {frequency} Hz to drive motor at {motor_rpm} rpm.')
        
        if run_calibration_forward:
            increment = 1
            self.emit_info_msg(f'Running calibration (forward)')
        elif run_calibration_backward:
            increment = -1
            self.emit_info_msg(f'Running calibration (backward)')
        elif run_manual:
            increment = 1 if forward else -1
            self.emit_info_msg(f'Running {"forward" if forward else "backward"} at {motor_spm:.2f} steps/min')
        else:
            raise Exception('switch_blocking: neither running calibration nor manual!')
        
        while True:
            if thread_stop_event.is_set() or self.stop_all_threads_event.is_set():
                break
            block_backward = self.switch_low.is_pressed
            block_forward = self.switch_high.is_pressed
            if run_calibration_forward and block_forward:
                self.calibration_finished = False
                self.absolute_position_high_limit = self.absolute_position
                self.potentio_current_high_limit = self.current_sensor.get_currentValue()
                self.logger.info(f'Forward calibration finished! absolute_position_high_limit = {self.absolute_position_high_limit}')
                self.switch_blocking(delay_on, delay_off, False, True, False, True, thread_stop_event)
                break
            if run_calibration_backward and block_backward:
                self.absolute_position_low_limit = self.absolute_position
                self.potentio_current_low_limit = self.current_sensor.get_currentValue()
                self.logger.info(f'Backward calibration finished! absolute_position_low_limit = {self.absolute_position_low_limit}')
                self.emit_info_msg(f'Stopped. Calibration finished with low and high limit position of {self.absolute_position_low_limit} and {self.absolute_position_high_limit} ({self.potentio_current_low_limit:.1f} and {self.potentio_current_high_limit:.1f} mA)')
                self.ui.signal_reset_mode.emit()
                self.ui.signal_calibration.emit(self.absolute_position_low_limit, self.absolute_position_high_limit)
                self.save_calibration_to_file()
                self.calibration_finished = True
                break
            if run_manual and forward and block_forward:
                self.logger.info('Switch triggered! Blocking forward motor movement')
                self.emit_info_msg('Stopped. Relay switching blocked! block_forward is set')
                self.ui.signal_reset_mode.emit()
                break
            if run_manual and not forward and block_backward:
                self.logger.info('Switch triggered! Blocking backward motor movement')
                self.emit_info_msg('Stopped. Relay switching blocked! block_backward is set')
                self.ui.signal_reset_mode.emit()
                break
            
            self.increment_position(increment, delay_on, delay_off)
            
        self.logger.info('Thread stopped!')
    
    def switch(self, delay_on, delay_off, run_calibration_forward=False, run_calibration_backward=False, forward=True):
        run_manual = not (run_calibration_forward or run_calibration_backward)
        self.stop_switching()
        self.thread_stop_event = threading.Event()
        args = (delay_on, delay_off, run_calibration_forward, run_calibration_backward, run_manual, forward, self.thread_stop_event)
        self.thread = threading.Thread(
            target = self.switch_blocking,
            args=args
        )
        self.thread.start()
        self.logger.info(f'Thread started! Args switch:\n{args}')
    
    def save_calibration_to_file(self):
        with open('data/calibration.txt', 'w') as f:
            f.write('Data: low and high limit of absolute position, low and high limit of current from potentiometer.\n')
            f.write(str(self.absolute_position_low_limit) + '\n')
            f.write(str(self.absolute_position_high_limit) + '\n')
            f.write(str(self.potentio_current_low_limit) + '\n')
            f.write(str(self.potentio_current_high_limit) + '\n')
    
    def get_position_percentage(self):
        return 100 * (self.absolute_position - self.absolute_position_low_limit) / (self.absolute_position_high_limit - self.absolute_position_low_limit)
    
    def get_potentio_position_percentage(self):
        if self.potentio_current_high_limit == self.potentio_current_low_limit:
            return -99 # Potentio not well calibrated
        return 100 * (self.current_sensor.get_currentValue() - self.potentio_current_low_limit) / (self.potentio_current_high_limit - self.potentio_current_low_limit)
    
    def run_cycles_blocking(self, delay_on, delay_off, cycle_min, cycle_max, cycle_time_hours, thread_stop_event):
        if not self.calibration_finished:
            self.emit_info_msg(f'Stopped. Run calibration before running cycles!')
            self.ui.signal_reset_mode.emit()
            return
        self.emit_info_msg(f'Running cycles {cycle_min}%-{cycle_max}% for {cycle_time_hours:.2f} hours')
        
        moving_forward = True
        start_time_s = time.time()
        if cycle_time_hours == 0:
            duration_s = float('inf')
        else:
            duration_s = int(3600*cycle_time_hours)
        
        while time.time()-start_time_s < duration_s:
            if thread_stop_event.is_set() or self.stop_all_threads_event.is_set():
                return
            
            if self.get_position_percentage() > cycle_max:
                moving_forward = False
            if self.get_position_percentage() < cycle_min:
                moving_forward = True
            
            block_backward = self.switch_low.is_pressed
            block_forward = self.switch_high.is_pressed
            if (block_forward and moving_forward) or (block_backward and not moving_forward):
                self.emit_info_msg(f'Stopped cycling. {"High" if block_forward else "Low"} limit switch triggered!')
                self.ui.signal_reset_mode.emit()
                return
            
            self.emit_info_msg(f'Running cycles {cycle_min}%-{cycle_max}% for {(duration_s - (time.time()-start_time_s))/3600:.2f} more hours.\nCurrent direction: {"forward" if moving_forward else "backward"}')
            
            self.increment_position(1 if moving_forward else -1, delay_on, delay_off)
        self.emit_info_msg(f'Stopped running cycles.')
        self.ui.signal_reset_mode.emit()
    
    def run_cycles(self, delay_on, delay_off, cycle_min, cycle_max, cycle_time_hours):
        self.stop_switching()
        self.thread_stop_event = threading.Event()
        args = (delay_on, delay_off, cycle_min, cycle_max, cycle_time_hours, self.thread_stop_event)
        self.thread = threading.Thread(
            target = self.run_cycles_blocking,
            args=args
        )
        self.thread.start()
        self.logger.info(f'Thread started! Args run_cycles:\n{args}')
        
    def stop_switching(self):
        if 'Stopped' not in self.info_msg:
            self.emit_info_msg('Stopped.')
        self.thread_stop_event.set()
        self.reset()


if __name__ == '__main__':
    logging.basicConfig(level='DEBUG', format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    
    relay_control = RelayControl()
    
    try:
        relay_control.switch(20, 10, False)
        time.sleep(2)
        relay_control.stop_switching()
    finally:
        relay_control.reset()
    
