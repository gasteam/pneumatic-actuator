from mainwindow import Ui_MainWindow
from PyQt5 import QtCore, QtGui, QtWidgets
import sys
from relay_control import RelayControl
from gpiozero import Button
import logging
import threading


# pyuic5 -x mainwindow.ui -o mainwindow.py

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    
    signal_info = QtCore.pyqtSignal(str)
    signal_reset_mode = QtCore.pyqtSignal()
    signal_position = QtCore.pyqtSignal(int, float, float)
    signal_calibration = QtCore.pyqtSignal(int, int)
    signal_current = QtCore.pyqtSignal(float)
    
    def __init__(self, relay_control):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.logger = logging.getLogger('MainWindow')
        self.logger.debug('Initializing MainWindow')
        relay_control.ui = self
        self.relay_control = relay_control
        self.comboBox_mode.currentIndexChanged.connect(lambda index: self.on_mode_changed(index))
        self.signal_info.connect(lambda info_msg: self.on_info_msg(info_msg))
        self.signal_reset_mode.connect(self.on_signal_reset_mode)
        self.signal_position.connect(lambda pos_abs, pos_perc, pos_potentio: self.on_signal_position(pos_abs, pos_perc, pos_potentio))
        self.signal_calibration.connect(lambda pos_low, pos_high: self.on_signal_calibration(pos_low, pos_high))
        self.signal_current.connect(lambda current: self.on_signal_current(current))
        
    def on_mode_changed(self, index):
        # Index:
        # [Off, calibration, manual, cycles]
        self.logger.debug(f'Mode changed to index {index}')
        use_times = self.comboBox_speed_mode.currentIndex()==0
        if use_times:
            delay_on  = self.doubleSpinBox_time_on.value()
            delay_off = self.doubleSpinBox_time_off.value()
        else:
            spm = self.doubleSpinBox_steps.value()
            percentage_on = self.spinBox_speed_percentage.value()
            delay_total = 1000/(spm/60)
            delay_on = percentage_on/100.0*delay_total
            delay_off = delay_total - delay_on
        
        if index == 0:
            relay_control.stop_switching()
        elif index == 1:
            relay_control.switch(delay_on, delay_off, run_calibration_forward=True)
        elif index == 2:
            forward = self.comboBox_direction.currentIndex()==0
            relay_control.switch(delay_on, delay_off, forward=forward)
        elif index == 3:
            cycle_min = self.spinBox_position_min.value()
            cycle_max = self.spinBox_position_max.value()
            cycle_time_hours = self.doubleSpinBox_time_cycles.value()
            relay_control.run_cycles(delay_on, delay_off, cycle_min, cycle_max, cycle_time_hours)
        else:
            relay_control.stop_switching()
            raise Exception(f'Unknown index {index}!')

    def on_info_msg(self, info_msg):
        self.label_status.setText(info_msg)
    
    def on_signal_reset_mode(self):
        self.comboBox_mode.setCurrentIndex(0)
    
    def on_signal_position(self, pos_abs, pos_perc, pos_potentio):
        text = f'Current position: step {pos_abs}'
        if pos_perc != -99:
            text += f' = {pos_perc:.1f}%'
        if pos_potentio != -99:
            text += f', potentio: {pos_potentio:.1f}%'
        self.label_position.setText(text)
    
    def on_signal_calibration(self, pos_low, pos_high):
        self.label_calibration.setText(f'Calibration active with low and high position = {pos_low} and {pos_high}')
    
    def on_signal_current(self, current):
        percentage = 100 * (current - 4) / 16.0
        self.label_current.setText(f'Current (4-20mA) = {current:.2f} mA = {percentage:.2f} %')



def app_exit_handler(stop_all_threads_event, relay_control):
    stop_all_threads_event.set()
    # Write absolute position to file
    with open('data/relay_absolute_position.txt', 'w') as f:
        f.write(str(relay_control.absolute_position))
    relay_control.write_counter_to_file()
    

if __name__ == '__main__':
    
    LOGGING_LEVEL = 'DEBUG'
    logging.basicConfig(level=LOGGING_LEVEL, format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    switch_low =  Button(2, bounce_time=0.2)
    switch_high = Button(3, bounce_time=0.2)
    stop_all_threads_event = threading.Event()
    relay_control = RelayControl(switch_low, switch_high, stop_all_threads_event)
    
    app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(lambda: app_exit_handler(stop_all_threads_event, relay_control))
    window = MainWindow(relay_control)
    window.relay_control.start_thread_current_display()
    window.relay_control.ui.signal_position.emit(window.relay_control.absolute_position, -99, -99)
    if window.relay_control.calibration_finished:
        window.relay_control.ui.signal_calibration.emit(
            window.relay_control.absolute_position_low_limit,
            window.relay_control.absolute_position_high_limit
        )
    window.show()
    app.exec()
    



